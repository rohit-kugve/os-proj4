compiling the module: make
Runnig the project:
*    Run the process you want to track, get its pid with the command "ps -e". Then insert module 
    with command "insmod ./driver.ko p=1234" where 1234 is the pid of the process you want to track
*   IF you want to track the process from its very first page fault, pass the name of the process instead of its process io with parameter "c=quicksort". After insmod, run the program quicksort.

Read the kernel logs with command "dmesg"

Graph for a quicksort run is in quicksort-graph.png
Explaination for the graph and data are in quicksort-data.xlsx
