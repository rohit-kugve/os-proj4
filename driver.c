/*
 * Here's a sample kernel module showing the use of jprobes to dump
 * the arguments of do_fork().
 *
 * For more information on theory of operation of jprobes, see
 * Documentation/kprobes.txt
 *
 * Build and insert the kernel module as done in the kprobe example.
 * You will see the trace data in /var/log/messages and on the
 * console whenever do_fork() is invoked to create a new process.
 * (Some messages may be suppressed if syslogd is configured to
 * eliminate duplicate messages.)
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>
#include <linux/proc_fs.h>
#include <linux/errno.h>    /* error codes */
#include <linux/types.h>    /* size_t */
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/time.h>

#define DATA_SIZE ( 1 * 1024 * 1024 )
#define MAX_CMD	30

int p = -1;	//pid to be passed to insmod
char *c = NULL; //command name to be passed to insmod
pid_t pid;
struct proc_dir_entry *proc_file_entry;
unsigned char *data = NULL; //buffer for proc data
int r = 0;  //read index;
int w = 0;  // write index
struct timespec tstart;
unsigned long long t_start = 0;
int time_started = 0;
module_param(p, int, 0);
module_param(c, charp, 0);

/*
 * Jumper probe for handle_mm_fault.
 * Mirror principle enables access to arguments of the probed routine
 * from the probe handler.
 */

int check_process(void)
{
	//check if the the current process is the one that we want to track
	//If user supplied a command name instead of pid, use it to check
	if( ( c != NULL ) && ( strncmp( current->comm, c, MAX_CMD)== 0) )	{
	//	printk(KERN_INFO "Matched %s = %s \n", current->comm, c);
		return  1;
	}
	//if user supplied pid, check against it
	if( pid == task_pid_nr(current) )
		return 1;
	//no match in either case	
	return 0;
}
char tmp_data[200];
int jod_handle_mm_fault(struct mm_struct *mm, struct vm_area_struct *vma,
                    unsigned long address, unsigned int flags)
{
    int len = 0, i = 0;
    struct timespec tend;
 	unsigned long long t_n = 0;	//absolute time in nanosec   	
	if( check_process() ){	//check if this is the process that we want to track
        tend = current_kernel_time();
        t_n  = (1000000000 * (unsigned long long)tend.tv_sec ) + ( tend.tv_nsec );       
        //printk(KERN_INFO "sec = %llu, nsec= %llu t_n= %llu\n", tend.tv_sec,tend.tv_nsec,t_n);
        printk(KERN_INFO "%s(%d):time in ns = %llu, addr = 0x%lx\n", current->comm, (int)task_pid_nr(current), t_n - t_start, address);
		len = sprintf(tmp_data,"%llu,%lu\n", t_n - t_start, address);
        for(i = 0; i < len; i++ )   {
            if( w == (DATA_SIZE - 2 ) ) {
	            //printk(KERN_INFO "proc buffer full\n");
                break;
            }
            else w++;
            	data[w] = tmp_data[i];
       	}	
    }
    jprobe_return();
	return 0;
}

static struct jprobe my_jprobe = {
	.entry  = jod_handle_mm_fault,
	.kp = {
		.symbol_name	= "handle_mm_fault",
	},
};

char msg[] = "msg from the depth\n";
int ptr = 0;
static ssize_t read_proc(struct file *file, char *buffer, size_t length, loff_t *offset)  
{
    if( w == 0 || r == w){ 
	    printk(KERN_INFO "buffer empty or read. w = %d\n", w);
        return 0;
    }
	else{
        copy_to_user(buffer, data+r, 1);
        r++;
        return 1;
    }
}

struct file_operations proc_fops = {
    .owner = THIS_MODULE,
    .read  = read_proc,
};
static int __init jprobe_init(void)
{
	int ret;
    pid = p;// copy the integer format pid into the pid_t type
	ret = register_jprobe(&my_jprobe);
	if (ret < 0) {
		printk(KERN_INFO "register_jprobe failed, returned %d\n", ret);
		return -1;
	}
	printk(KERN_INFO "Planted jprobe at %p, handler addr %p for process = %s\n",
	       my_jprobe.kp.addr, my_jprobe.entry, c);
    
    proc_file_entry = proc_create("jprobe", 0666, NULL, &proc_fops);
    if(proc_file_entry == NULL) {
        printk(KERN_INFO "proc file creation failed\n");
	    unregister_jprobe(&my_jprobe);
        return -ENOMEM;
    }
    data = kmalloc( DATA_SIZE, GFP_KERNEL);
    if( data == NULL )    {
        printk(KERN_INFO "malloc failed\n");
        //unregister jprobe before returning error
	    unregister_jprobe(&my_jprobe);
	    remove_proc_entry("jprobe",NULL);
        return -ENOMEM;
    }
    memset(data, 0, DATA_SIZE);
    tstart = current_kernel_time();
    t_start  = (1000000000 *(unsigned long long) tstart.tv_sec ) + ( tstart.tv_nsec );       
    printk(KERN_INFO "AT START sec = %llu, nsec= %llu t_n= %llu\n", (unsigned long long)tstart.tv_sec,(unsigned long long)tstart.tv_nsec,(unsigned long long)t_start);
    return 0;
}

static void __exit jprobe_exit(void)
{
	unregister_jprobe(&my_jprobe);
	printk(KERN_INFO "jprobe at %p unregistered\n", my_jprobe.kp.addr);
	remove_proc_entry("jprobe",NULL);
    if( data != NULL )
        kfree(data);
}

module_init(jprobe_init)
module_exit(jprobe_exit)

MODULE_LICENSE("GPL");
