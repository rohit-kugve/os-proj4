#include<stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(void)
{
    unsigned long i = 0, rounds = 0;
    unsigned long size = 10 * 4  * 1024;
    unsigned char *p = NULL;
    while(1)    {
//        printf("round %lu, allocating %lu bytes\n", rounds, size);
        p = malloc( size );
        if( p == NULL ){
            printf("malloc failed\n");
            exit(0);
        }
 
        for ( i = 0; i < size; i++ ){
            p[i] = i + rounds;
            usleep(1);
        }
       // free(p);
        p = NULL;
        rounds++;
    }
    return 0;
}
